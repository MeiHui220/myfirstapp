import { Dish } from './dish';

export const DISHES: Dish[] = [
    {
      id: '0',
      name: 'pizza',
      image: '/assets/images/pizza.jpg',
      category: 'mains',
      featured: true,
      label: 'Hot',
      price: '4.99',
      // tslint:disable-next-line:max-line-length
      description: 'A unique combination of Indian Uthappam (pancake) and Italian pizza, topped with Cerignola olives, ripe vine cherry tomatoes, Vidalia onion, Guntur chillies and Buffalo Paneer.',
      comments: [
        {
            rating: 5,
            comment: 'Imagine all the eatables, living in conFusion!',
            author: 'John Lemon',
            date: '2012-10-16T17:57:28.556094Z'
        },
        {
            rating: 4,
            comment: 'Sends anyone to heaven, I wish I could get my mother-in-law to eat it!',
            author: 'Paul McVites',
            date: '2014-09-05T17:57:28.556094Z'
        },
        {
            rating: 3,
            comment: 'Eat it, just eat it!',
            author: 'Michael Jaikishan',
            date: '2015-02-13T17:57:28.556094Z'
        },
    ]  
    },
    {
      id: '1',
      name: 'donut',
      image: '/assets/images/donut.jpg',
      category: 'appetizer',
      featured: false,
      label: '',
      price: '1.99',
      description: 'Deep fried Zucchini coated with mildly spiced Chickpea flour batter accompanied with a sweet-tangy tamarind sauce',
      comments: [
        {
            rating: 5,
            comment: 'Imagine all the eatables, living in conFusion!',
            author: 'John Lemon',
            date: '2012-10-16T17:57:28.556094Z'
        },
        {
            rating: 4,
            comment: 'Sends anyone to heaven, I wish I could get my mother-in-law to eat it!',
            author: 'Paul McVites',
            date: '2014-09-05T17:57:28.556094Z'
        },
        {
            rating: 3,
            comment: 'Eat it, just eat it!',
            author: 'Michael Jaikishan',
            date: '2015-02-13T17:57:28.556094Z'
        },
    ]
    },
  ];