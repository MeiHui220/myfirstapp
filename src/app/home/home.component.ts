import { Component, OnInit } from '@angular/core';
import { Dish } from '../shared/dish';
import { Data } from '../shared/datas';
import { DishService } from '../services/dish.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  dish: Dish;
  data: any;
  columnDefs = [
    {field: 'bl_fi_mst_comp.code', sortable: true},
    {field: 'bl_fi_mst_comp.name', sortable: true},
    {field: 'bl_fi_mst_comp.abbreviation', sortable: true},
    {field: 'bl_fi_mst_comp.comp_registration_num', sortable: true},
    {field: 'bl_fi_mst_comp.ccy_code', sortable: true}
  ];

  constructor(
    private http: HttpClient,
    private dishservice: DishService) { }


  ngOnInit(): void {
    // this.dish = this.dishservice.getFeaturedDish();
    const httpHeaders: HttpHeaders = new HttpHeaders({
      Authorization: 'eyJhbGciOiJSUzI1NiJ9.eyJzdWJqZWN0R3VpZCI6IjVlOTk4OTViLWYxYzAtNDM2NC1iNzk4LWIyMDQwYWQ4ZWEwZSIsImFwcEd1aWQiOiIiLCJ0ZW5hbnRHdWlkIjoiIiwiYW5vbnltb3VzIjpmYWxzZSwiZXhwIjoxNjA0MjIwODYxfQ.FNnk8Y7B-z1eNWqwlV4fHd6EjHCwXopt6fZcPLXGjHSnjftUlu1qhmPdR2JxaRRWvT9XxutMdtY1UNGARq2jCOOqyMmQxxUGAHMLKgrWjFmpTLf7UwGcJ2v4JalumAp6LhA0TKsn6ULitQ5mg8UgrrF6jVIhyAT9vkruzaep9sE',
      tenantCode: 'tnt_hassan_code',
    });
    this.http.get<Data>('https://api-test.akaun.com/core2/dm/companies?Authorization=eyJhbGciO&tenantCode=tnt_hassan_code',
      { headers: httpHeaders })
    .subscribe(
      (data: Data) => {
      this.data = data.data;
      console.log(this.data);
    },
      error => {
        console.log(error);
      });
  }

}
